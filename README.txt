This plugin uses the Confluence Notification API to allow administrators to send messages to all users of a Confluence wiki.

You can find the entire tutorial at: 

https://developer.atlassian.com/display/CONFDEV/Posting+notifications+in+Confluence