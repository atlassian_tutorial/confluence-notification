package com.example.plugins.tutorial.confluence.notification.resource;

import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.mywork.model.Notification;
import com.atlassian.mywork.model.NotificationBuilder;
import com.atlassian.mywork.service.LocalNotificationService;
import com.atlassian.user.impl.DefaultGroup;
import com.atlassian.user.search.page.Pager;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;


/**
 * A resource of creating and listing notifications
 */
@Path ("/")
@Consumes ((MediaType.APPLICATION_JSON)) // prevents XSRF !
public class NotificationResource
{

    public static final int MAX_RESULT = 3;
    public static final String PLUGIN_KEY = "com.example.plugins.tutorial.confluence.notification";
    private final LocalNotificationService notificationService;
    private final UserAccessor userAccessor;
    private final PermissionManager permissionManager;

    public NotificationResource(final LocalNotificationService notificationService, final UserAccessor userAccessor, final PermissionManager permissionManager)
    {
        this.notificationService = notificationService;
        this.userAccessor = userAccessor;
        this.permissionManager = permissionManager;
    }

@POST
public Response createNotification(@FormParam ("title") String title, @FormParam ("message") String message)
        throws Exception
{
    if (isAdmin())
    {
        sendNotificationToAllUsers(title, message);
        return Response.ok().build();
    }
    else
    {
        return Response.status(Response.Status.FORBIDDEN).build();
    }
}

@GET
@Produces (MediaType.APPLICATION_JSON)
public Response findAllNotifications() throws Exception{
    if (isAdmin()) {
        // find all the notifications received by the logged in user
        final Iterable<Notification> notifications = notificationService.findAll(AuthenticatedUserThreadLocal.getUser().getName());
        Iterables.filter(notifications, new Predicate<Notification>()
        {
            @Override
            public boolean apply(@Nullable final Notification notification)
            {
                // we are only interested in the notification send by our plugin
                return PLUGIN_KEY.equals(notification.getApplication());
            }
        });
        // Let's only display the last MAX_RESULT notifications
        return Response.ok(Iterables.limit(notifications, MAX_RESULT)).build();
    }
    else {
        return Response.status(Response.Status.FORBIDDEN).build();
    }
}

    private boolean isAdmin()
    {
        return permissionManager.isConfluenceAdministrator(AuthenticatedUserThreadLocal.getUser());
    }

/**
 * Iterate on all users of the "confluence-user" group and send a notification to each of them
 * @param title the title of the notification to send
 * @param message the body of the notification to send
 * @throws ExecutionException
 * @throws InterruptedException
 */
private void sendNotificationToAllUsers(final String title, final String message)
        throws ExecutionException, InterruptedException
{
    Pager<String> memberNames = userAccessor.getMemberNames(new DefaultGroup(UserAccessor.GROUP_CONFLUENCE_USERS));
    for (String memberName : memberNames)
    {
        sendNotification(memberName, title, message);
    }
}

/**
 * Create a single notification and send it to user
 * @param user the user who will receive the notification
 * @param title the title of the notification
 * @param message the body of the notification
 * @return the created notification
 * @throws InterruptedException
 * @throws ExecutionException
 */
private Notification sendNotification(final String user, final String title, final String message) throws InterruptedException, ExecutionException{
    Notification notification = notificationService.createOrUpdate(user, new NotificationBuilder()
            .application(PLUGIN_KEY) // a unique key that identifies your plugin
            .title("Message from your beloved administrator")
            .itemTitle(title)
            .description(message)
            .groupingId("com.example.plugins.tutorial.confluence.notification") // a key to aggregate notifications
            .createNotification()).get();
    return notification;
}

}